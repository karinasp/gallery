import 'package:flutter/material.dart';

/// Returns text with giving parametrs
Widget textField(String text, FontWeight weight, Color color, double num) {
  return Align(
    alignment: Alignment.centerLeft,
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Text(
        text,
        style: TextStyle(
            color: color,
            fontFamily: 'Overpass',
            fontWeight: weight,
            fontSize: num),
      ),
    ),
  );
}
