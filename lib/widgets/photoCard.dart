import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'textField.dart';

/// Returns photos displayed in SingleChildScrollView
Widget photo(List<dynamic> listPhotos, BuildContext context) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 15),
    color: Colors.black87,
    child: StaggeredGridView.count(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      crossAxisCount: 4,
      crossAxisSpacing: 10.0,
      mainAxisSpacing: 10.0,
      staggeredTiles: listPhotos.map((item) {
        return StaggeredTile.fit(2);
      }).toList(),
      children: listPhotos.map((item) {
        // item onTap
        return InkWell(
          onTap: () {
            Navigator.of(context).pushNamed('\ImagePage', arguments: item);
          },
          child: Container(
            decoration: BoxDecoration(
               // Adds rounded corners to a item card
                borderRadius: BorderRadius.circular(18),
                gradient: RadialGradient(
                    center: Alignment.bottomRight,
                    radius: 0.5,
                    colors: [Color(0xfff5f8fd), Color(0xfff5f8fd)])),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(18),
                    ),
                    color: Colors.grey[300],
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Hero(
                          tag: item["portrait"],
                          // Adds rounded corners to a given item
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(18),
                              topRight: Radius.circular(18),
                            ),
                            child: CachedNetworkImage(
                              imageUrl: item["portrait"],
                              placeholder: (context, url) => Container(
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 5, top: 5),
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // add description
                      item["alt_description"] != ''
                          ? textField(
                              "${item["alt_description"]}",
                              FontWeight.normal,
                              Colors.black,
                              12,
                            )
                          : SizedBox(
                              height: 0,
                            ),
                      SizedBox(
                        height: 5,
                      ),
                      // add photographer
                      textField(
                        "Photographer: ",
                        FontWeight.bold,
                        Colors.black,
                        13,
                      ),
                      textField(
                        "${item["photographer"]}",
                        FontWeight.normal,
                        Colors.black,
                        13,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }).toList(),
    ),
  );
}
