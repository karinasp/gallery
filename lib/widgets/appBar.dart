import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Returns App Bar
AppBar appBar = AppBar(
  elevation: 0.0,
  backgroundColor: Colors.black,
  title: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text('Gallery', style: TextStyle(color: Colors.white, fontSize: 20)),
      Text('Flutter', style: TextStyle(color: Colors.lightBlue)),
    ],
  ),
  backwardsCompatibility: false,
  systemOverlayStyle: SystemUiOverlayStyle(
    statusBarColor: Colors.black,
    statusBarIconBrightness: Brightness.light,
  ),
);
