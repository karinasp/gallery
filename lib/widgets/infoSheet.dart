import 'package:flutter/material.dart';

/// Bottom-sheet displaying info for a given [photo].
class InfoSheet extends StatelessWidget {
  final Map photo;

  InfoSheet(this.photo);

  @override
  Widget build(BuildContext context) => Container(
        height: 140,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(color: Colors.black87),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildAuthorWidget(photo['photographer']),
            _buildDescriptionWidget(photo['alt_description']),
          ].toList(),
        ),
      );

  /// Builds widget displaying a given [author] for an photo.
  Widget _buildAuthorWidget(String author) => Padding(
        padding: const EdgeInsets.only(
            left: 26.0, right: 16.0, top: 8.0, bottom: 16.0),
        child: Text(
          'Photographer: $author',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            letterSpacing: 0.1,
          ),
        ),
      );

  /// Builds widget displaying a given [description] for an photo.
  Widget _buildDescriptionWidget(String description) => Padding(
        padding: const EdgeInsets.only(
            left: 26.0, right: 16.0, top: 8.0, bottom: 16.0),
        child: Text(
          description != '' ? 'Description: $description' : 'No description',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            letterSpacing: 0.1,
          ),
        ),
      );
}
