import 'dart:convert';
import 'package:http/http.dart' as http;

/// Helper class to interact with the Unsplash Api
class ApiServices {
  Future getTrendingPhotos(int page, String apiKEY) async {
    dynamic response;
    try {
      await http
          .get(Uri.parse(
              "https://api.unsplash.com/photos/?client_id=$apiKEY&per_page=30&page=$page"))
          .then((value) {
        response = jsonDecode(value.body);
      });
      return response;
    } catch (error) {
      print("Error Getting Trending Photos: ($error)");
      return null;
    }
  }
}
