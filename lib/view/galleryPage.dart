import 'package:flutter/material.dart';
import '../widgets/appBar.dart';
import '../controller/getPhotos.dart';
import '../widgets/photoCard.dart';

/// Screen for showing a collection of trending photos
class GalleryPage extends StatefulWidget {
  const GalleryPage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

/// Provide a state for [GalleryPage]
class _HomePageState extends State<GalleryPage> {
  ScrollController _scrollController = new ScrollController();
  GetPhotos _getPhotos = GetPhotos();
  int page = 1;
  List photos = [];
  
  @override
  void initState() {
    _getPhotos.getTrendingPhotos(page).then((value) {
      setState(() {
        photos = value;
      });
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        page++;
        _getPhotos.getTrendingPhotos(page).then((value) {
          setState(() {
            photos.addAll(value);
          });
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: [
            photo(photos, context),
          ],
        ),
      ),
    );
  }
}