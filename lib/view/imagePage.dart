import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import '../widgets/infoSheet.dart';

/// Screen for showing one photo
class ImagePage extends StatefulWidget {
  final Map photo;

  ImagePage({required this.photo});

  @override
  _ImagePageState createState() => _ImagePageState();
}

/// Provide a state for [ImagePage]
class _ImagePageState extends State<ImagePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late PersistentBottomSheetController infoBottomSheetController;

  /// Returns AppBar
  Widget _buildAppBar() => AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        leading:
            // back button
            IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                onPressed: () => Navigator.pop(context)),
        actions: <Widget>[
          // show photo info
          IconButton(
            icon: Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            tooltip: 'Image Info',
            onPressed: () => infoBottomSheetController = _showInfoBottomSheet(),
          ),
        ],
      );

  /// Returns PhotoView around [widget.photo]
  Widget _buildPhotoView(String imageId, String imageUrl) => Hero(
        tag: imageId,
        child: PhotoView(
          imageProvider: NetworkImage(imageUrl),
          initialScale: PhotoViewComputedScale.covered,
          minScale: PhotoViewComputedScale.covered,
          maxScale: PhotoViewComputedScale.covered,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          widget.photo['big'] != null
              ? _buildPhotoView(widget.photo['id'], widget.photo['big'])
              : _buildPhotoView(widget.photo['id'], 'assets/images/img.jpg'),
          Positioned(top: 0.0, left: 0.0, right: 0.0, child: _buildAppBar()),
        ],
      ),
    );
  }

  /// Shows a InfoSheet containing photo info
  PersistentBottomSheetController _showInfoBottomSheet() {
    return _scaffoldKey.currentState!
        .showBottomSheet((context) => InfoSheet(widget.photo));
  }
}
