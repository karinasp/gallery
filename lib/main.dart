import 'package:flutter/material.dart';
import '../controller/routes.dart';

void main() {
  runApp(WallPaperFlutter());
}

class WallPaperFlutter extends StatelessWidget {
  const WallPaperFlutter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gallery Flutter',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.white),
      initialRoute: '\GalleryPage',
      onGenerateRoute: Routes.routesGenerater,
    );
  }
}
