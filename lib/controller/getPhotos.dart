import '../models/services.dart';
import '../config/keys.dart';

/// Get List with photos info from JSON
class GetPhotos {
  String apiKEY = Keys.UNSPLASH_API_CLIENT_ID;
  ApiServices _apiServices = ApiServices();
  Future getTrendingPhotos(int page) async {
    List photos = [];
    dynamic response = await _apiServices.getTrendingPhotos(page, apiKEY);
    if (response != null) {
      response.forEach((element) {
        photos.add({
          "id": element["id"],
          "link": element["links"]["self"],
          "photographer": element["user"]["name"].toString().contains('null')
              ? 'Unknown Photographer'
              : element["user"]["name"],
          "portrait": element["urls"]["small"],
          "big": element['urls']['full'],
          "created_at": element["created_at"],
          "alt_description":
              element["alt_description"].toString().contains('null')
                  ? ''
                  : element["alt_description"],
          "likes": element["likes"],
        });
      });
      return photos;
    } else {
      return null;
    }
  }
}
