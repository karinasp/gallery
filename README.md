# GALLERY APP #

A simple Flutter app showing trending images from Unsplash

### USE ###

* REST API
* StatefulWidget/StatelessWidget
* HTTP
* Route
* Futures

### SCREENSHOTS ###


![photo_1](screenshots/photo_2021-10-30_21-29-19.jpg) ![photo_2](screenshots/photo_2021-10-30_21-29-22.jpg)
